```
Note: Before moving forward follow the below instructions on this file 
Replace 
- <h1> with #
- <h2> with 2#'s
- <h3> with 3#'s

Remove above text 

```

<h1> Instructions for ${artifactId}

<h1> Table of Contents 

- [Reference API Proxy](#reference-api-proxy)
- [Description](#description)
- [Purpose](#purpose)
- [Pre-Requisites](#pre-requisites)
- [Usage Instructions](#usage-instructions)
- [Lifecycle for API proxy deployment](#lifecycle-for-api-proxy-deployment)
    - [Lint Proxy](#lint-proxy)
    - [Unit Tests](#unit-tests)
    - [Bundle Package](#bundle-package)
    - [Upload Caches](#upload-caches)
    - [Upload Target Servers](#upload-target-servers)
    - [Upload KVM's](#upload-kvms)
    - [Deploy Proxy](#deploy-proxy)
    - [Upload Products](#upload-products)
    - [Upload Developers](#upload-developers)
    - [Upload Apps](#upload-apps)
    - [Export Keys](#export-keys)
    - [Upload to Artifactory](#upload-to-artifactory)

<h2> Description 

While deploying a API proxy to any targeted Apigee Edge env, it must go through with the several CI
phases such as Linting, Code Coverage, Unit testing, Build and packaging, configuring environment
level dependencies (KVM's, Caches, Target Servers), Configuring org level entities(API products,
Developers, Apps) and integration testing. Also, artifacts must be deploy to the remote locations
(Nexus).

<h2> Purpose 

The DevOps engineer should be able to execute all the above mentioned CI phases by manually running
the steps.

<h2> Pre-Requisites 
- JDK 8
- Apache Maven
- Linux/WSL
- Settings.xml file in local 
- SAML Tokens (Access & Refresh)

<h2> Usage Instructions

This Api Proxy project will be used to deploy \${artifactId} to apigee edge.

How to clone the \${artifactId} and change the directory

```bash
# clone ${artifactId} repository
git clone https://scmserver/..../${artifactId}.git

# change directory into the repository
cd ${artifactId}
```

<h2> Lifecycle for API proxy deployment 

to make local invocation easier, load the app token from an environment variable

example of how to export a local environment variable:

```bash
export APIGEE_USERNAME=username@broadcom.com
export APIGEE_PWD=provide_your_apigee_edge_ui_password_here
```

<h3> Lint Proxy

```
# invoke apigeelint directly
apigeelint -s edge/apiproxy -f html.js

#invoke linting through maven; run from /edge directory in project root
cd edge
mvn test -Pproxy-linting
```

<h3> Unit Tests 

```
# run from /edge directory in project root
mvn test -Pproxy-unit-test
```

<h3> Bundle Package 

run this from `edge` directory:

```bash
mvn package \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName}
```

<h3> Upload Caches 

```bash
mvn apigee-config:caches \
    -Papigee  \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}-${envName}.json \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD

```

<h3> Upload Target Servers 

```bash
mvn apigee-config:targetservers \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}-${envName}.json

```

<h3> Upload KVM's 

```bash
mvn apigee-config:keyvaluemaps \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}-${envName}.json
```

<h3> Deploy Proxy 

```bash
mvn apigee-enterprise:deploy \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD

```

<h3> Upload Products 

```bash
mvn apigee-config:apiproducts \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}.json \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD

```

<h3> Upload Developers 

```bash
mvn apigee-config:developers \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}.json \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD

```

<h3> Upload Apps 

```bash
mvn apigee-config:apps \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}.json \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD \
```

<h3> Export Keys 

```bash
mvn apigee-config:exportAppKeys \
    -Papigee \
    -Denv=${envName} \
    -Dorg=${orgName} \
    -Dapigee.config.file=target/resources/edge/envConfig-${orgName}-${envName}.json \
    -Dapigee.config.options=create \
    -Dusername=$APIGEE_USERNAME -Dpassword=$APIGEE_PWD \
    -Dapigee.config.exportDir=target/test/integration

```

<h3> Upload to Artifactory 

```bash
Disable for developers. Only CI Server can deploy to the remote location.
```
