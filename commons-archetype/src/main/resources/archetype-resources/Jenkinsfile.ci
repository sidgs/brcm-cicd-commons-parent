#!groovy

/**
 * Jenkinsfile to be used for CI for Apigee CI Pipelines
 */

properties([[$class: 'BuildDiscarderProperty',
             strategy: [$class: 'LogRotator', numToKeepStr: '10']]])

def branch_type = get_branch_type "${env.BRANCH_NAME}"

if ( branch_type == 'dev') {
    apigeeProxyCiRunner 'develop', env.BUILD_NUMBER
} else if (branch_type == 'release') {
    apigeeProxyCiRunner  'release', env.BUILD_NUMBER
} else if ( branch_type == 'feature') {
    apigeeProxyCiRunner  'feature', env.BUILD_NUMBER
} else if ( branch_type == 'hotfix') {
    apigeeProxyCiRunner  'hotfix', env.BUILD_NUMBER
} else {
    node {
        stage (${ env.BRANCH_NAME }) {
            echo 'CICD not Enabled'
        }
    }
}

// Utility functions
def get_branch_type(String branch_name) {
    //Must be specified according to <flowInitContext> configuration of jgitflow-maven-plugin in pom.xml
    def dev_pattern = '.*develop'
    def release_pattern = '.*rel-.*'
    def feature_pattern = '.*feat-.*'
    def hotfix_pattern = '.*hf-.*'
    def master_pattern = '.*master'
    if (branch_name =~ dev_pattern) {
        return 'dev'
    } else if (branch_name =~ release_pattern || branch_name == 'release') {
        return 'release'
    } else if (branch_name =~ master_pattern) {
        return 'master'
    } else if (branch_name =~ feature_pattern) {
        return 'feature'
    } else if (branch_name =~ hotfix_pattern) {
        return 'hotfix'
    } else {
        return ''
    }
}
